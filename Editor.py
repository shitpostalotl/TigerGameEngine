import pickle

class room():
	def __init__(self, name, description, contents):
		self.name = name # The name of the room.
		self.description = description # The room's description. Displayed under the name.
		self.contents = contents # A list of the things inside the room.
class thing():
	def __init__(self, name, description, cangrab, combines):
		self.name = name # A thing's name, displayed in lists and typed by the player.
		self.description = description # The thing's description.
		self.cangrab = cangrab # A Boolean value for whether or not the thing can be grabbed by the player.
		self.combines = combines # A dictionary of things it can be combined with, in the format {combineItem : resultItem}
class box(thing):
	def __init__(self, name, description, contents, multiple, key, cangrab = False, combines = {}):
		thing.__init__(self, name, description, cangrab, combines)
		self.contents = contents # The item that the box has inside of it.
		self.multiple = multiple # A Boolean value for if the box has more then one item in it.
		self.key = key # An item that is required to get the contents from the box. Set to None if the box is not locked.
		self.used = False # Don't worry about this.
class door(thing):
	def __init__(self, name, description, destination, key, cangrab = False, combines = {}):
		thing.__init__(self, name, description, cangrab, combines)
		self.destination = destination # The place that the door will lead to, in the format "roomID".
		self.key = key # Equivalent to the same-named argument in the Box class.

# Define things here \/ \/ \/
door0 = door("First Door", "The door to the second room", "room1", None)
door1 = door("Second Door", "The door to the first room", "room0", None)
cube = thing("Cube", "A really cool cube. All there really is to say about it.", True, {})
room0 = room("First Room", "A room. The first one, in fact.", [door0])
room1 = room("Second Room", "A room. The second one, in fact.", [door1,cube])
# Define things here /\ /\ /\

rooms = {
    "room0": room0,
	"room1": room1
} # An index of rooms, in the format {"roomID" : room}
pickle.dump(rooms, open("data/rooms.tger", "wb"))

currentRoom = room0 # The room that the player starts in.
pickle.dump(currentRoom, open("data/currentRoom.tger", "wb"))

invanory = [] # The inventory that the player starts with.
pickle.dump(invanory, open("data/inventory.tger", "wb"))

title = {
	"title": "Tiger Engine Testing Demo",
	"author": "shitpostalotl"
} # The game's title and the author's name
pickle.dump(title, open("data/title.tger", "wb"))
