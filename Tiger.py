import pickle, os, Engine

# VARIABLES
titleScrean = pickle.load(open("data/title.tger", "rb"))

'''if os.path.isfile("save/rooms.tger") == True:
	if input("Load saved files? [Y/N] ").lower() == "y": load = "save"
	else: load = "data"
else: load = "data"

global rooms
global inventory
global currentRoom

rooms = pickle.load(open(load+"/rooms.tger", "rb"))
inventory = pickle.load(open(load+"/inventory.tger", "rb"))
currentRoom = pickle.load(open(load+"/currentRoom.tger", "rb"))'''

command = ""
items = []

# GAMELOOP
while command != "q":
	if os.name == "posix": os.system('clear') # Unix/Linux/MacOS/BSD/etc
	elif os.name in ("nt", "dos", "ce"): os.system('CLS') # DOS/Windows
	else: print("\n" * os.get_terminal_size().lines, end='') # Fallback for other operating systems

	print(titleScrean["title"]+"\n	 by "+titleScrean["author"])
	print("~~~~~~~~~~\nYou are in the "+currentRoom.name+".\n"+currentRoom.description)
	print("~~~~~~~~~~\n"+currentRoom.name+" contains the following "+str(len(currentRoom.contents))+" things: ")
	for i in currentRoom.contents: print(i.name+": "+i.description+"\n~~~~~~~~~~")
	
	if inventory != []:
		print("~~~~~~~~~~\nInventory contains the following "+str(len(inventory))+" things: ")
		for i in inventory: print(i.name+": "+i.description)
	else: print("Your inventory is empty.")

	items.extend(currentRoom.contents)
	items.extend(inventory) # Create list of items in the current room/pocket

	print("~~~~~~~~~~\n[g]: Put a thing in your inventory/use boxes; [c]: Combine one item with another (the first one must be in your inventory); [p]: Go through a passageway; [q]: Quit the game.")

	if command == "g": Engine.grab(scanRoom(input("get what? "))) # grab an item
	elif command == "c": Engine.combine(scanInv(input("Combine what? ")), scanAll(input("With what? "))) # combine two items
	elif command == "p": Engine.passageway(scanRoom(input("Which passageway? "))) # use a door to go to another room

	try:
		import msvcrt
		command = msvcrt.getch()
	except ImportError:
		import sys, tty, termios
		fd = sys.stdin.fileno()
		old_settings = termios.tcgetattr(fd)
		try:
			tty.setraw(sys.stdin.fileno())
			ch = sys.stdin.read(1)
		finally:
			termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
		command =  ch

if input("Save game state? (previous saves will be overwritten) [Y/N]\n") == "Y":
	if os.path.isfile("save/placeholdersavefile") == True: os.remove("save/placeholdersavefile")
	print("Saving rooms, inventory, and location to disk.")
	pickle.dump(rooms, open("save/rooms.tger", "wb"))
	pickle.dump(inventory, open("save/inventory.tger", "wb"))
	pickle.dump(currentRoom, open("save/currentRoom.tger", "wb"))
print("quitting...")
