Tiger.py:

This file contains all the code to actually run every Tiger Engine game by reading all the assets and data from the .TGER ("dot-tiger") files located in data/.

Editor.py:

This file is the development environment for Tiger Engine games. You simply define all the objects that the game world is made of, then run the script. It will then save all the game world data into data/, allowing Tiger.py to run it. You do not need to have Editor.py to be able to play Tiger Engine games.

save/:

This is where the save data from the player playing the game is stored.



## todo

1) Seperate the functions that do things from the TUI ->DEBUG WHATEVER BROKE THIS<-
2) Make UIs for Tiger.py: [pysimplegui](https://pysimplegui.readthedocs.io), [brython](https://brython.info/), and screen-reader acessible.
3) Make a GUI for Editor.py
