import pickle, os

# CLASSES
class room():
	def __init__(self, name, description, contents):
		self.name = name
		self.description = description
		self.contents = contents

class thing():
	def __init__(self, name, description, cangrab, combines):
		self.name = name
		self.description = description
		self.cangrab = cangrab
		self.combines = combines

class box(thing):
	def __init__(self, name, description, contents, multiple, key, cangrab = False, combines = {}):
		thing.__init__(self, name, description, cangrab, combines)
		self.contents = contents
		self.multiple = multiple
		self.key = key
		self.used = False

class door(thing):
	def __init__(self, name, description, destination, key, cangrab = False, combines = {}):
		thing.__init__(self, name, description, cangrab, combines)
		self.destination = destination
		self.key = key

# FUNCTIONS
# scanRoom function
def scanRoom(itemName):
	for i in currentRoom.contents:
		if i.name == itemName: return(i)

# scanInv function
def scanInv(itemName):
	for i in inventory:
		if i.name == itemName: return(i)

# scan inv and room function
def scanAll(itemName):
	for i in items:
		if i.name == itemName: return(i)

# function to grab items
def grab(active):
	if active != None:
		if isinstance(active, box) == False:
			if active.cangrab == True:
				inventory.append(active)
				currentRoom.contents.remove(active)
				print("Got "+active.name)
			elif active.cangrab == False: print("You can't get the "+active.name)
		elif isinstance(active, box) == True and ((active.key == None) or (active.key in inventory)):
			if active.multiple == False:
				if active.used == False:
					inventory.append(active.contents)
					print("Got "+active.contents.name+" from "+active.name)
					active.used = True
				elif active.used == True: print("The "+active.name+" is empty.")
			elif active.multiple == True:
				if active.contents not in inventory:
					inventory.append(active.contents)
					print("Got "+active.contents.name)
				elif active.contents in inventory: print("You already have a "+active.contents.name+" in your inventory.")
		elif active.key not in inventory: print("The "+active.name+" is locked.")
	else: print("That thing isn't in the room.")

# functions to combine two items
def combine(active1, active2):
	try:
		inventory.append(active1.combines[active2.name])
		print("Got "+(active1.combines[active2.name]).name)
		inventory.remove(active1)
		if active2 in inventory: inventory.remove(active2)
		elif active2 in currentRoom.contents: currentRoom.contents.remove(active2)
	except AttributeError: print("You cannot combine those items. One or both of them may not exist.") 

# go through a door
def passageway(active):
	if active != None and isinstance(active, door) == True:
		if active.key == None or active.key in inventory: currentRoom = rooms[active.destination]
		else: print("The door is locked.")
	else: print("That door does not exist.")

# VARIABLES
if os.path.isfile("save/rooms.tger") == True and input("Load saved files? [Y/N] ").lower() == "y": load = "save"
else: load = "data"

global rooms
global inventory
global currentRoom

rooms = pickle.load(open(load+"/rooms.tger", "rb"))
inventory = pickle.load(open(load+"/inventory.tger", "rb"))
currentRoom = pickle.load(open(load+"/currentRoom.tger", "rb"))
